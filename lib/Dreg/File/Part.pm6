use v6;

class Part {
    has $.file-name;
    has $!file-handle;

    submethod TWEAK() {
        $!file-handle  = open $!file-name;
    }

    method sequences(--> Seq) {
        $!file-handle.lines.split(/\n/);
    }

    #method next() {
    #    for $!file-handle.lines.split(/\n/) { .say }
    #}
}
